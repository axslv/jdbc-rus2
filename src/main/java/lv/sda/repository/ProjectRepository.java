package lv.sda.repository;

import lv.sda.DbUtils;
import lv.sda.entity.Department;
import lv.sda.entity.Project;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {

    public static List<Project> findAll() {
        List<Project> projectList = new ArrayList<>();
        String sql = "SELECT * FROM projects";
//try-with-resources
        try (
                Connection conn = DriverManager.getConnection(DbUtils.DB_HOST, DbUtils.DB_USERNAME, DbUtils.DB_PASSWORD);
                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);

        ) {

            while (rs.next()) {
                Project p = new Project();
                p.setId(rs.getInt("id"));
                p.setDescription(rs.getString("description"));
                projectList.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projectList;
    }

    public static List<Project> findById(Integer id) {
        List<Project> projects = new ArrayList<>();
        String sql = "SELECT * FROM projects WHERE id = ?"; //    SELECT * FROM projects WHERE id = id

        try  {
            Connection conn = DriverManager.getConnection(DbUtils.DB_HOST, DbUtils.DB_USERNAME, DbUtils.DB_PASSWORD);
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                Project em = new Project();

                em.setId(rs.getInt("id"));
                em.setDescription(rs.getString("description"));
                projects.add(em);
            }
            rs.close();
            statement.close();
            conn.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return projects;

    }

}
