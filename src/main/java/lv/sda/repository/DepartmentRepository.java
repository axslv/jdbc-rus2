package lv.sda.repository;

import lv.sda.DbUtils;
import lv.sda.entity.Department;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentRepository {
    private static int count = 0;

    public static List<Department> findAll() {
        List<Department> departmentList = new ArrayList<>();
        String sql = "SELECT * FROM departments";
//try-with-resources
        try (
                Connection conn = DriverManager.getConnection(DbUtils.DB_HOST, DbUtils.DB_USERNAME, DbUtils.DB_PASSWORD);
                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);

        ) {

            while (rs.next()) {
                Department em = new Department();

                em.setDepartmentId(rs.getInt("employeeId"));
                em.setName(rs.getString("name"));
                departmentList.add(em);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return departmentList;
    }

    public static List<Department> findByFirstLetterName(String letter) {
        List<Department> employees = new ArrayList<>();
        String sql = "SELECT * FROM employees WHERE firstName LIKE ?"; //    SELECT * FROM employees WHERE firstName LIKE 'J%'

        try  {
            Connection conn = DriverManager.getConnection(DbUtils.DB_HOST, DbUtils.DB_USERNAME, DbUtils.DB_PASSWORD);
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, letter + "%");
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                Department em = new Department();

                em.setDepartmentId(rs.getInt("employeeId"));
                employees.add(em);
            }
            rs.close();
            statement.close();
            conn.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return employees;

    }

    public static int save(Department department) {
        String sql = "INSERT INTO `departments` (`name`) VALUES (?)";

        try  {
            Connection conn = DriverManager.getConnection(DbUtils.DB_HOST, DbUtils.DB_USERNAME, DbUtils.DB_PASSWORD);
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, department.getName());
            int rs = statement.executeUpdate();
            count = rs;

            statement.close();
            conn.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;

    }

    public static int update(Department department) {
        String sql = "UPDATE departments SET name=? WHERE departmentId = ?";

        try  {


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;

    }

    public static List<Department> findByName(String letter) {
        List<Department> employees = new ArrayList<>();
        String sql = "SELECT * FROM departments WHERE name = ?"; //    SELECT * FROM employees WHERE firstName LIKE 'J%'

        try  {




        } catch (SQLException e) {
            e.printStackTrace();
        }

        return employees;

    }



}
