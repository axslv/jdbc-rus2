package lv.sda.repository;

import lv.sda.DbUtils;
import lv.sda.entity.Employee;
import lv.sda.entity.Project;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeRepository {

    public static List<Employee> findAll() {
        List<Employee> employeeList = new ArrayList<>();
        String sql = "SELECT * FROM employees";
//try-with-resources
        try (
                Connection conn = DriverManager.getConnection(DbUtils.DB_HOST, DbUtils.DB_USERNAME, DbUtils.DB_PASSWORD);
                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);

        ) {

            while (rs.next()) {
                Employee em = new Employee();

                em.setEmployeeId(rs.getInt("employeeId"));
                em.setFirstName(rs.getString("firstName"));
                em.setLastName(rs.getString("lastName"));
                em.setPhoneNumber(rs.getString("phoneNumber"));
                em.setEmail(rs.getString("email"));

                employeeList.add(em);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employeeList;
    }

    public static List<Employee> findByFirstLetterName(String letter) {
        List<Employee> employees = new ArrayList<>();
        String sql = "SELECT * FROM employees WHERE firstName LIKE ?"; //    SELECT * FROM employees WHERE firstName LIKE 'J%'

        try  {
            Connection conn = DriverManager.getConnection(DbUtils.DB_HOST, DbUtils.DB_USERNAME, DbUtils.DB_PASSWORD);
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, letter + "%");
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                Employee em = new Employee();

                em.setEmployeeId(rs.getInt("employeeId"));
                em.setFirstName(rs.getString("firstName"));
                em.setLastName(rs.getString("lastName"));
                em.setPhoneNumber(rs.getString("phoneNumber"));
                em.setEmail(rs.getString("email"));

                employees.add(em);
            }


        } catch (SQLException e) {
            e.printStackTrace();

        }
        return employees;

    }

}
