package lv.sda;

import lv.sda.entity.Department;
import lv.sda.entity.Employee;
import lv.sda.entity.Project;
import lv.sda.repository.DepartmentRepository;
import lv.sda.repository.EmployeeRepository;
import lv.sda.repository.ProjectRepository;

import java.util.List;

public class Main {


    public static void main(String[] args) {
        // 1.
        List<Project> allProjects = ProjectRepository.findAll();

        for (Project p : allProjects) {
            System.out.println(p.getId() + ", " + p.getDescription());
        }

        // 2.
        List<Employee> employees = EmployeeRepository.findAll();
        for (Employee e : employees) {
            System.out.println(e.getEmployeeId() + ", " + e.getFirstName() + ", " + e.getLastName() + ", " + e.getEmail());
        }

        // 3.
        System.out.println("--------------------------------------------------------");
        List<Employee> employees1 = EmployeeRepository.findByFirstLetterName("J");
        for (Employee e : employees1) {
            System.out.println(e.getEmployeeId() + ", " + e.getFirstName() + ", " + e.getLastName() + ", " + e.getEmail());
        }

        //2b
        System.out.println("***********************************************");
        List<Project> projects = ProjectRepository.findById(1);
        for (Project p : projects) {
            System.out.println(p.getId() + ", " + p.getDescription());
        }

        //2d
        System.out.println("///////////////////////////////////////////////");
        Department d = new Department();
        d.setName("Another department");
        int save = DepartmentRepository.save(d);
        System.out.println(save);

    }

}
